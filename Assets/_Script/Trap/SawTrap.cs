using UnityEngine;

namespace _Script.Trap
{
    public class SawTrap : MonoBehaviour
    {
        [SerializeField] private GameObject[] wayPoints;
        [SerializeField] private Transform saw;
        [SerializeField] private float speed = 2f;

        private int _currentPointIndex;

        private void Update()
        {
            // Vector2.Distance -> tra ve khoang cach giua 2 vector
            if (Vector2.Distance(wayPoints[_currentPointIndex].transform.position, saw.position) < .1f)
            {
                _currentPointIndex++;
                if (_currentPointIndex >= wayPoints.Length)
                {
                    _currentPointIndex = 0;
                }
            }

            saw.position = Vector2.MoveTowards(
                saw.position, // vi tri hien tai
                wayPoints[_currentPointIndex].transform.position, // dich den
                speed * Time.deltaTime // khoang cach toi da di chuyen dc o moi frame
            );
        }
    }
}