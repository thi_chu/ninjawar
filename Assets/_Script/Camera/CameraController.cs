using System;
using UnityEngine;

namespace _Script.Camera
{
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private Vector2 minBounds; // vi tri min cua cam
        [SerializeField] private Vector2 maxBounds; // vi tri max cua cam

        private Vector3 offset = new Vector3(0, 0, -10f);
        private Vector3 velocity = Vector3.zero;
        private float smoothTime = 0.25f;

        /// <summary>
        /// Mathf.Clamp dung de gioi han gia tri so tron 1 khoang cu the max => value => min.
        /// o day dung de han che di chuyen cua camera trong 1 pham vi nhat dinh là minBounds và maxBounds
        /// </summary>
        private void LateUpdate()
        {
            
            Vector3 targetPos = target.position + offset;
            targetPos.x = Mathf.Clamp(target.position.x,minBounds.x,maxBounds.x);
            targetPos.y = Mathf.Clamp(target.position.y,minBounds.y,maxBounds.y);
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
        }
    }
}