using _Script.UI;
using UnityEngine;

namespace _Script.Enemy
{
    public class EnemyStatus : MonoBehaviour
    {
        [SerializeField] private Collider2D enemyColl;
        [SerializeField] private int maxHp = 50;
        [SerializeField]  private EnemyHeathBar heathBar;
        [SerializeField]  private GameObject canvas;
        [SerializeField] private AudioSource dieAudio;

        private Animator _animator;
        private int _currentHp;
        private bool _isHurt = false;

        // Start is called before the first frame update
        void Start()
        {
            _animator = GetComponent<Animator>();
            _currentHp = maxHp;
            heathBar.UpdateHeathBar(_currentHp,maxHp);
            canvas.SetActive(false);
        }

        /// <summary>
        /// nhan sat thuong tu player & giam hp
        /// </summary>
        /// <param name="dmg"></param>
        public void ReceiverDamage(int dmg)
        {
            this._currentHp = _currentHp - dmg;
            if (!this.IsDead())
            {
                canvas.SetActive(true);
                _animator.SetTrigger("Hurt");
                _isHurt = true;
            }
            else
            {
                canvas.SetActive(false);
                _currentHp = 0;
                _animator.SetTrigger("Die");
                enemyColl.enabled = false;
                dieAudio.Play();
            }
            heathBar.UpdateHeathBar(_currentHp,maxHp);
        }


        public bool IsDead()
        {
            return this._currentHp <= 0;
        }

        public bool IsHurt()
        {
            return _isHurt;
        }

        public void CanMove()
        {
            _isHurt = false;
        }

        private void OnDestroy()
        {
            Destroy(gameObject);
        }
    }
}