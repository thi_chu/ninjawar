using UnityEngine;

namespace _Script.Enemy
{
    public class EnemyMovement : MonoBehaviour
    {
        [Header("Patrol Points")]
        [SerializeField] private Transform leftEdge;
        [SerializeField] private Transform rightEdge;

        [Header("Enemy")]
        [SerializeField] private Transform enemy;
        [SerializeField] private Animator animator;
        [SerializeField] private EnemyStatus status;

        [Header("Enemy Movement")]
        [SerializeField] private float speed;

        [SerializeField] private float idleDuration;
        private float _idleTimer;

        private Vector3 _initScale;
        private bool _isMovingLeft;


        private void Awake()
        {
            _initScale = enemy.localScale;
        }

        private void OnDisable()
        {
            if (enemy == null)
                return;
            animator.SetBool("Moving", false);
        }

        // Update is called once per frame
        void Update()
        {
            if (enemy == null)
                return;
            if (_isMovingLeft == true)
            {
                if (enemy.position.x >= leftEdge.position.x)
                {
                    MoveInDirection(-1);
                }
                else
                {
                    DirectionChange();
                }
            }
            else
            {
                if (enemy.position.x <= rightEdge.position.x)
                {
                    MoveInDirection(1);
                }
                else
                {
                    DirectionChange();
                }
            }
        }

        private void MoveInDirection(int direction)
        {
            if (status.IsHurt())
            {
                _idleTimer += Time.deltaTime;

                if (!(_idleTimer >= idleDuration))
                {
                    animator.SetBool("Moving", false);
                    return;
                }
               
                status.CanMove();
                MoveEnemy(direction);
            }
            else
            {
                MoveEnemy(direction);
            }
           
        }

        private void MoveEnemy(int direction)
        {
            _idleTimer = 0;
            animator.SetBool("Moving", true);

            // xoay mat enemy theo huong da xac dinh
            // Mathf.Abs() se lay gia tri tuyet doi cua param
            enemy.localScale = new Vector3(Mathf.Abs(_initScale.x) * direction, _initScale.y, _initScale.z);

            // di chuyen theo huong da xac dinh
            enemy.position = new Vector3(
                enemy.position.x + Time.deltaTime * direction * speed,
                enemy.position.y,
                enemy.position.z);
        }

        private void DirectionChange()
        {
            animator.SetBool("Moving", false);
            _idleTimer += Time.deltaTime;

            if (_idleTimer >= idleDuration)
            {
                _isMovingLeft = !_isMovingLeft;
            }
        }
    }
}