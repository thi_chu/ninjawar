using _Script.Player;
using UnityEngine;

namespace _Script.Enemy
{
    public class EnemyAttacking : MonoBehaviour
    {
        [SerializeField] private float attackCooldown;
        [SerializeField] private float range;
        [SerializeField] private float boxCollDistance;
        [SerializeField] private BoxCollider2D boxColl;
        [SerializeField] private int damage = 5;
        [SerializeField] private LayerMask playerLayer;
        [SerializeField] private AudioSource attackAudio;

        private float _cooldownTimer = Mathf.Infinity;
        [SerializeField] Animator animator;
        private PlayerStatus _playerHeath;
        [SerializeField]EnemyMovement enemyPatrol;

        // Update is called once per frame
        void Update()
        {
            _cooldownTimer += Time.deltaTime;

            // chi tan cong khi player o trong pham vi tan cong
            if (PlayerInSight() && !_playerHeath.IsDead())
            {
                if (_cooldownTimer >= attackCooldown)
                {
                    _cooldownTimer = 0;
                    animator.SetTrigger("Attack");
                    attackAudio.Play();
                }
            }

            // chi di chuyen khi player khong o trong pham vi tan cong
            if (enemyPatrol != null)
            {
                enemyPatrol.enabled = !PlayerInSight();
            }
        }

        // check palyer co dung trong pham vi tan cong ko
        private bool PlayerInSight()
        {
            var bound = boxColl.bounds;
            var originHitbox = bound.center + transform.right * (range * transform.localScale.x * boxCollDistance);
            var sizeHitbox = new Vector3(bound.size.x * range, bound.size.y, bound.size.z);
            RaycastHit2D hitBox = Physics2D.BoxCast(
                originHitbox,
                sizeHitbox,
                0,
                Vector2.left,
                0,
                playerLayer);
            if (hitBox.collider != null)
            {
                _playerHeath = hitBox.transform.GetComponent<PlayerStatus>();
            }

            return hitBox.collider != null;
        }

        private void OnDrawGizmos()
        {
            var bound = boxColl.bounds;
            var originHitbox = bound.center + transform.right * (range * transform.localScale.x * boxCollDistance);
            var sizeHitbox = new Vector3(bound.size.x * range, bound.size.y, bound.size.z);
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(originHitbox, sizeHitbox);
        }

        /// <summary>
        /// khi animation attack chạy tới vị tri tan cong thi tru hp player
        /// </summary>
        private void DamagePlayer()
        {
            if (PlayerInSight() && !_playerHeath.IsDead())
            {
                _playerHeath.ReceiverDamage(damage);
            }
        }
    }
}