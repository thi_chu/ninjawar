using UnityEngine;

namespace _Script.UI
{
   public class VitoryFlag : MonoBehaviour
   {
      [SerializeField] private Animator anim;
      [SerializeField] private AudioSource vitorySource;
      [SerializeField] private GameObject gameOverPanel;
      private void OnTriggerEnter2D(Collider2D coll)
      {
         if (!coll.CompareTag("Player")) return;
         anim.SetTrigger("Win");
         vitorySource.Play();
         Invoke(nameof(GameOver),3.5f);
      }

      public void GameOver()
      {
         gameOverPanel.SetActive(true);
      }
   }
}
