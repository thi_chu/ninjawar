using System;
using UnityEngine;

namespace _Script.UI
{
    public class Gem : MonoBehaviour
    {
        [SerializeField] private int amount;
        [SerializeField] private GemManager manager;

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (!coll.CompareTag("Player")) return;
            Destroy(gameObject);
            manager.UpdateGem(amount);
            manager.PlayAudio();
        }
    }
}
