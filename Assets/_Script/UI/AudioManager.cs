using UnityEngine;
using UnityEngine.UI;

namespace _Script.UI
{
    public class AudioManager : MonoBehaviour
    {

        [Header("Music")] 
        [SerializeField] private Text musicText;
        [SerializeField] private Slider musicSlider;
        [SerializeField] private AudioSource musicSounds;

        [Header("SFX")] 
        [SerializeField] private Text sfxText;
        [SerializeField] private Slider sfxSlider;
        [SerializeField] private AudioSource[] sfxSounds;

        private void Awake()
        {
            musicSlider.minValue = 0;
            musicSlider.maxValue = 1;
            sfxSlider.minValue = 0;
            sfxSlider.maxValue = 1;

            if (PlayerPrefs.HasKey("MusicVolume") == false && PlayerPrefs.HasKey("SFXVolume") == false)
            {
                PlayerPrefs.SetFloat("MusicVolume", 0.6f);
                PlayerPrefs.SetFloat("SFXVolume", 0.5f);
            }

            musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");
            sfxSlider.value = PlayerPrefs.GetFloat("SFXVolume");
            SetVolumeMusic(musicSlider.value);
            SetVolumeSfx(sfxSlider.value);
        }

        private void OnGUI()
        {
            sfxText.text = (int) (sfxSlider.value * 100) + " %";
            musicText.text = (int) (musicSlider.value * 100) + " %";
        }

        private void Update()
        {
            SetVolumeMusic(musicSlider.value);
            SetVolumeSfx(sfxSlider.value);
        }
        

        public void SetVolumeMusic(float volume)
        {
            musicSounds.volume = volume;
            PlayerPrefs.SetFloat("MusicVolume", volume);
        }

        public void SetVolumeSfx(float volume)
        {
            foreach (var sfx in sfxSounds)
            {
                sfx.volume = volume;
            }

            PlayerPrefs.SetFloat("SFXVolume", volume);
        }
    }
}