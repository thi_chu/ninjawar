using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Script.UI
{
    public class GemManager : MonoBehaviour
    {
        [SerializeField] private Text gemCount;
        [SerializeField] private AudioSource itemSource;

        private int _gem = 0;

        private void OnGUI()
        {
            gemCount.text = _gem + "/5";
        }

        public void UpdateGem(int amount)
        {
            _gem += amount;
        }

        public void PlayAudio()
        {
            itemSource.Play();
        }
    }
}
