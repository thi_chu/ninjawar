using System;
using _Script.Player;
using UnityEngine;

namespace _Script.UI
{
    public class HeathItem : MonoBehaviour
    {
        [SerializeField] private float heathValue;

        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (!coll.CompareTag("Player")) return;
            coll.GetComponent<PlayerStatus>().AddHeath(heathValue);
            Destroy(gameObject);
        }
    }
}