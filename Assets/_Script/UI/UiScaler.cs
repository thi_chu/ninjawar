﻿using UnityEngine;
using UnityEngine.UI;

namespace _Script.UI
{
    public class UiScaler : MonoBehaviour
    {
        public float baseWidth;
        public float baseHeight;
        [SerializeField] private CanvasScaler scaler;
        public float raito;
        private void Awake()
        {
            var w = Screen.width / baseWidth;
            var h = Screen.height / baseHeight;
            raito = w / h;
            raito = raito >= 1f ? 1f : 0;
            scaler.matchWidthOrHeight = raito;
        }
    }
}
