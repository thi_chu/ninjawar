using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Script.UI
{
    public class StartMenu : MonoBehaviour
    {
        public void StartGame()
        {
            SceneManager.LoadScene(1);
        }
        
        public void Quit()
        {
            Application.Quit();
        }
    }
}
