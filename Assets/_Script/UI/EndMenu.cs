using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Script.UI
{
   public class EndMenu : MonoBehaviour
   {
      [SerializeField] private AudioSource musicSounds;
      private void LateUpdate()
      {
         if (gameObject.activeSelf)
         {
            musicSounds.Stop();
         }
      }

      public void Restart()
      {
         SceneManager.LoadScene(1);
      }

      public void MainMenu()
      {
         SceneManager.LoadScene(0); 
      }
      public void Quit()
      {
         Application.Quit();
      }
   }
}
