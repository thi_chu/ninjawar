using UnityEngine;
using UnityEngine.UI;

namespace _Script.UI
{
    public class PlayerHeathBar : MonoBehaviour
    {
        [SerializeField] private Image heathBarCurrent;
        [SerializeField] private AudioSource heathSource;

        public void UpdateHeathBar(int currentHeath, int maxHeath)
        {
            heathBarCurrent.fillAmount = (float) currentHeath / maxHeath;
        }
        
        public void PlayAudio()
        {
            heathSource.Play();
        }
    }
}