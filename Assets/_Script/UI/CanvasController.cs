using System;
using UnityEngine;

namespace _Script.UI
{
    public class CanvasController : MonoBehaviour
    {
        
        [SerializeField] private GameObject volumeSetting;
        [SerializeField] private GameObject settingButton;

        public void OpenSettingButton()
        {
            settingButton.SetActive(true);
        }
        public void OpenVolumeSetting()
        {
            volumeSetting.SetActive(true);
        }
        
        public void CloseSettingButton()
        {
            settingButton.SetActive(false);
        }
        public void CloseVolumeSetting()
        {
            volumeSetting.SetActive(false);
        }
    }
}
