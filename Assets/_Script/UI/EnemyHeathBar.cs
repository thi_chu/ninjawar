using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Script.UI
{
    public class EnemyHeathBar : MonoBehaviour
    {
        [SerializeField] private Image heathBarCurrent;

        public void UpdateHeathBar(int currentHeath, int maxHeath)
        {
            heathBarCurrent.fillAmount = (float) currentHeath / maxHeath;
        }
    }
}
