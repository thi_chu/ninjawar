using UnityEngine;

namespace _Script.Player
{
    public class PlayerMovement : MonoBehaviour
    {
        private Rigidbody2D _rgb2D;
        private BoxCollider2D _coll2D;
        public SpriteRenderer sprite;
        private Animator _anim;


        private float _posX = 0;
        private bool _isDoubleJump = false;
        private bool _isMoveLeft = false;
        private bool _isMoveRight = false;

        [SerializeField] private float runSpeed = 7f;
        [SerializeField] private float jumpPoce = 7f;
        [SerializeField] private LayerMask groundLayer;

        private enum MovermentSate
        {
            Idle,
            Running,
            Jumping,
            Falling,
        }

        private MovermentSate _sate;

        /// <summary>
        /// khoi tao cac tai nguyen truoc khi vao first frame
        /// </summary>
        private void Awake()
        {
            _rgb2D = GetComponent<Rigidbody2D>();
            _coll2D = GetComponent<BoxCollider2D>();
            sprite = GetComponent<SpriteRenderer>();
            _anim = GetComponent<Animator>();
        }

        // Update is called once per frame
        void Update()
        {
            MovePlayer();
            _rgb2D.velocity = new Vector2(_posX,_rgb2D.velocity.y);
            OnFlipX();
            PlayerAnimationSateUpdate();
        }

        /// <summary>
        /// update animation di chuyen cua player
        /// </summary>
        private void PlayerAnimationSateUpdate()
        {
            /* animation running bat khi _pox != 0
             * animation jumping chay khi _rgb2D.velocity.y != 0
             * su dung enum de chay animtion bang SetInteger.
             */
            if (CheckGround())
            {
                if (_posX != 0)
                {
                    _sate = MovermentSate.Running;
                }
                else
                {
                    _sate = MovermentSate.Idle;
                }
            }

            if (_rgb2D.velocity.y > .1f)
            {
                _sate = MovermentSate.Jumping;
            }
            else if (_rgb2D.velocity.y < -.1f)
            {
                _sate = MovermentSate.Falling;
            }

            _anim.SetInteger("MovementSate", (int) _sate);
        }

        private bool CheckGround()
        {
            var bound = _coll2D.bounds;
            RaycastHit2D hit = Physics2D.BoxCast(
                bound.center,
                bound.size,
                0,
                Vector2.down,
                0.1f,
                groundLayer);

            return hit;
        }

        // check khi _pox <0 lat hinh anh player sang trai va nguoc lai.
        private void OnFlipX()
        {
            if (_posX > 0)
            {
                sprite.flipX = false;
            }
            else if (_posX < 0)
            {
                sprite.flipX = true;
            }
        }

        public void JumpButton()
        {
            if (CheckGround())
            {
                _rgb2D.velocity = new Vector2(_rgb2D.velocity.x, jumpPoce);
                _isDoubleJump = false;
                PlayerController.Instance.jumpAudio.Play();
            }
            else if (_isDoubleJump == false)
            {
                _rgb2D.velocity = new Vector2(_rgb2D.velocity.x, jumpPoce * .75f);
                _isDoubleJump = true;
                PlayerController.Instance.jumpAudio.Play();
            }
        }

        private void MovePlayer()
        {
            if (_isMoveLeft)
            {
                _posX = -runSpeed;
            }
            else if (_isMoveRight)
            {
                _posX = runSpeed;
            }
            else
            {
                _posX = 0;
            }
        }

        public void MoveLeftButtonDown()
        {
            _isMoveLeft = true;
        }

        public void MoveLeftButtonUp()
        {
            _isMoveLeft = false;
        }

        public void MoveRightButtonDown()
        {
            _isMoveRight = true;
        }

        public void MoveRightButtonUp()
        {
            _isMoveRight = false;
        }
    }
}