using _Script.Enemy;
using UnityEngine;

namespace _Script.Player
{
    public class PlayerMeleeAttack : MonoBehaviour
    {
        [Header("Player")] 
        [SerializeField] private SpriteRenderer sprite;

        [Header("Melee Attack")] [SerializeField]
        private int damage = 10;

        [SerializeField] private float attackRadius = 1f;
        [SerializeField] private LayerMask enemyLayer;
        [SerializeField] private GameObject  swordAtkLeft;
        [SerializeField] private GameObject swordAtkRight;


        private Animator _animator;
        private static readonly int SwordAtk = Animator.StringToHash("SwordAtk");
        private float _attackCooldown;
        private float _cooldownTime = .5f;

        void Awake()
        {
            _animator = GetComponent<Animator>();
            
        }

        // Update is called once per frame
        void Update()
        {
            _attackCooldown += Time.deltaTime;
        }

        private void DamageEnemy()
        {
            var pos = !sprite.flipX
                ? swordAtkRight.transform.position
                : swordAtkLeft.transform.position;
            Collider2D enemy = Physics2D.OverlapCircle(pos, attackRadius, enemyLayer);
            if (enemy)
            {
                enemy.GetComponent<EnemyStatus>().ReceiverDamage(damage);
            }
        }


        /// <summary>
        /// ve bieu dien pham vi tan cong
        /// </summary>
        private void OnDrawGizmos()

        {
            Gizmos.DrawWireSphere(swordAtkRight.transform.position, attackRadius);

            Gizmos.DrawWireSphere(swordAtkLeft.transform.position, attackRadius);
        }

        public void AttackButotn()
        {
            if (!(_attackCooldown >= _cooldownTime)) return;
            _animator.SetTrigger(SwordAtk);
            PlayerController.Instance.swordSkillAudio.Play();
            _attackCooldown = 0;
        }
    }
}