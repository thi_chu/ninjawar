using System.Collections.Generic;
using UnityEngine;

namespace _Script.Player
{
    public class PlayerAttacking : MonoBehaviour
    {
        [Header("Player")] 
        [SerializeField] private SpriteRenderer sprite;
        [SerializeField] private Animator anim;

        // set skill button 
        [Header("Fire Ball")] 
        [SerializeField] private GameObject firePointRight;
        [SerializeField] private GameObject firePointLeft;
        [SerializeField] private GameObject fireball;
        [SerializeField] private List<GameObject> fireballList;


        private float _attackCooldown = 0;
        private float _cooldownTime = .75f;

        private static readonly int FireAtk = Animator.StringToHash("FireAtk");

        // Start is called before the first frame update
        void Start()
        {
            fireballList = new List<GameObject>();
        }

        // Update is called once per frame
        void Update()
        {
            _attackCooldown += Time.deltaTime;
        }

        private void PlayerAnimationAttackUpdate()
        {
            anim.SetTrigger(FireAtk);
            PlayerController.Instance.fireSkillAudio.Play();
            SkillAttack();
            _attackCooldown = 0;
        }


        private void SkillAttack()
        {
            var pos = !sprite.flipX
                ? firePointRight.transform.position
                : firePointLeft.transform.position;

            GameObject doFireBall;
            if (!sprite.flipX)
            {
                doFireBall = Instantiate(this.fireball, pos, firePointRight.transform.rotation);
            }
            else
            {
                doFireBall = Instantiate(this.fireball, pos, firePointLeft.transform.rotation);

                //xoay hinh anh flreball theo toa do x
                var fireBallScale = firePointLeft.transform.localScale;
                fireBallScale = new Vector3(-fireBallScale.x, fireBallScale.y, fireBallScale.z);
                doFireBall.transform.localScale = fireBallScale;
            }

            this.fireballList.Add(doFireBall);
        }

        public void SkillButton()
        {
            if (_attackCooldown >= _cooldownTime)
            {
                PlayerAnimationAttackUpdate();
            }
        }
    }
}