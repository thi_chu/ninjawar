using System;
using UnityEngine;

namespace _Script.Player
{
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController Instance;
        [Header("audio")] 
        public AudioSource fireSkillAudio;
        public AudioSource swordSkillAudio;
        public AudioSource jumpAudio;
        public AudioSource deadAudio;

        private void Start()
        {
            Instance = this;
        }
    }
}