using _Script.Enemy;
using UnityEngine;

namespace _Script.Player
{
    public class FireballAttack : MonoBehaviour
    {
        [SerializeField] private float speed = 3;

        private float _lifeTime;
        private Animator _animator;

        private static readonly int Explode = Animator.StringToHash("Explode");

        private void Awake()
        {
            _animator = GetComponent<Animator>();
        }

        private void Update()
        {
            // di chuyen fileball theo huong da xac dinh
            var movementSpeed = speed * Time.deltaTime * transform.localScale.x;
            transform.Translate(movementSpeed, 0, 0);

            /*
             * qua thoi gian quy dinh ma chua va cham se huy fireball
             */
            _lifeTime += Time.deltaTime;
            if (_lifeTime > 1.25f)
            {
                _animator.SetTrigger(Explode);
                Deactivate();
            }
        }

        private void OnTriggerEnter2D(Collider2D coll)
        {
            switch (coll.tag)
            {
                case "Enemy":
                    _animator.SetTrigger(Explode);
                    coll.GetComponent<EnemyStatus>().ReceiverDamage(10);
                    break;
                default:
                    return;
            }
        }

        /// xoa fireball
        private void Deactivate()
        {
            Destroy(gameObject);
        }
    }
}