using _Script.UI;
using UnityEngine;

namespace _Script.Player
{
    public class PlayerStatus : MonoBehaviour
    {
        [SerializeField] private int maxHp = 100;
        [SerializeField]  private PlayerHeathBar heathBar;
        [SerializeField]  private PlayerMovement movement;
        [SerializeField] private GameObject gameOverPanel;


        private Animator _animator;
        private Rigidbody2D _rgb2D;
        private Collider2D _coll;
        private int _currentHp;

        private void Start()
        {
            _animator = GetComponent<Animator>();
            _rgb2D = GetComponent<Rigidbody2D>();
            _coll = GetComponent<Collider2D>();
            _currentHp = maxHp;
            heathBar.UpdateHeathBar(_currentHp, maxHp);
        }

        /// <summary>
        /// nhan sat thuong tu enemy & giam hp
        /// </summary>
        /// <param name="dmg"> hp mat di tuong duong sat thuong nhan vao</param>
        public void ReceiverDamage(int dmg)
        {
            _currentHp -= dmg;
            if (!this.IsDead())
            {
                _animator.SetTrigger("Hurt");
            }
            else
            {
                _currentHp = 0;
                _animator.SetTrigger("Die");
                movement.enabled = false;
                PlayerController.Instance.deadAudio.Play();
                _coll.enabled = false;
                _rgb2D.constraints = RigidbodyConstraints2D.FreezeAll;
                // player dead -> dung game & hien thi man hinh game over 
                Invoke(nameof(GameOver),1.5f);
            }
            heathBar.UpdateHeathBar(_currentHp, maxHp);
        }


        public bool IsDead()
        {
            return this._currentHp <= 0;
        }

        /// <summary>
        /// hoi mau cho player
        /// </summary>
        /// <param name="heathPoint"> luong ho hoi phuc</param>
        public void AddHeath(float heathPoint)
        {
            //Mathf.Clamp
            // Trả về giá trị đã cho nếu nó nằm trong phạm vi tối thiểu và tối đa.
            // - value: gia tri tra ve
            // - min: gia tri toi thieu
            // - max: gai tri toi da
            _currentHp = (int) Mathf.Clamp(_currentHp+heathPoint, 0, maxHp);
            heathBar.UpdateHeathBar(_currentHp, maxHp);
            heathBar.PlayAudio();
        }
        
        private void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.CompareTag("Trap"))
            {
                ReceiverDamage(10);
            }
        }

        public void GameOver()
        {
            gameOverPanel.SetActive(true);
        }
        
    }
}